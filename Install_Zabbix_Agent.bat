:CheckOS
IF EXIST "%PROGRAMFILES(X86)%" (set arch=win64) ELSE (set arch=win32)

net stop "Zabbix Agent"
Robocopy "\\somefileserver\Zabbix-Windows-Deployment\zabbix_agent" "C:\zabbix_agent" /MIR /COPYALL

cd /D c:\
c:\zabbix_agent\bin\%arch%\zabbix_agentd.exe -d
c:\zabbix_agent\bin\%arch%\zabbix_agentd.exe --config=c:\zabbix_agent\zabbix_agentd.conf --install

net start "Zabbix Agent"
pause
