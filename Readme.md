Zabbix-Windows-Deployment
=========

The project will facilitate installing Zabbix agents on Windows systems
either manually or via policy.

Setup
-----------

* Clone the project to a share on your server

```sh
git clone https://genebean@bitbucket.org/genebean/zabbix-windows-deployment.git Zabbix-Windows-Deployment
```

* Change the file path from \\somefileserver to your server's path in Install_Zabbix_Agent.bat
* If running via policy, remove the pause on the last line of Install_Zabbix_Agent.bat
* Download the latest agent from zabbix.com and copy the bin folder to zabbix_agent
* In the provied zabbix_agentd.conf, set Server and ServerActive to your Zabbix server's dns name or ip

Running
-----------

Either right click and run as admin or run via GPO at boot.

License
-----------

BSD 3-clause (see LICENSE for details)

